const express = require("express")
const router = express.Router()
const Users = require("../models/users")
// Хеширование паролей
const bcrypt = require('bcryptjs')
const salt = bcrypt.genSaltSync(10);

// Файлы 
const multer = require('multer')
const upload = multer({ dest: 'uploads/' })

router.post("/uploadAvatar/id", upload.single('photo'), async (req, res) => {
    try {
        // После загрузки файла - у нас появится переменная req.file 
        let file = req.file

        file.path

        // Users.findByIdAndUpdate(req.params)

        console.log(file);
    } catch (error) {

    }

})


router.patch('/avatar/:id', upload.single("file"), async (req, res) => {
    try {
        let updatedUser = { avatar: req.file.path }

        Users.findOneAndUpdate(req.params.id, updatedUser, (error, data) => {
            if (data) {
                res.json({
                    message: "Update updateD",
                    ok: true
                })
            }
        })
    } catch (error) {
        console.log(error)
    }
})





router.get("/", async (req, res) => {
    try {
        Users.find({}, (error, data) => {
            res.json({
                ok: true,
                message: 'Пользователи получены',
                body: data
            })
        })

    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.get("/:id", async (req, res) => {
    try {
        let users = await Users.findById(req.params.id)

        res.json({
            ok: true,
            message: 'Пользователь получен',
            data: users
        })

    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.patch("/:id", async (req, res) => {
    try {
        Users.findByIdAndUpdate(req.params.id, req.body, (error, data) => {
            if (error) {
                console.log(error);

                res.json({
                    ok: false,
                    message: "Error inside callback function WTF!",
                    error
                })
            } else {
                res.json({
                    ok: true,
                    message: "Element patch!",
                    element: data
                })
            }
        })
    }
    catch (error) {
        res.json({
            ok: false,
            message: "Seems there nogoods!",
            error
        })
    }
})

// Попытка войти в аккаунт
router.post("/login", async (req, res) => {
    try {
        // Попытка войти в аккаунт (match)
        // login
        Users.findOne({ "email": req.body.email }, (error, data) => {
            if (error) {
                res.json({
                    ok: false,
                    message: "Some error!"
                })
            } else {
                if (data == null) {
                    res.json({
                        ok: false,
                        message: "No user! Fuck you!"
                    })
                } else {
                    // LOGIN IS OK
                    let isPasswordCorrect = bcrypt.compareSync(req.body.password, data.password)

                    // PASSWORD IS OK
                    if (isPasswordCorrect) {
                        res.json({
                            ok: true,
                            message: "Signed In"
                        })

                        return
                    }

                    // PASSWORD IS WRONG
                    res.json({
                        ok: false,
                        message: "Wrong Password! Login is OK!"
                    })
                }
            }
        })

        // Верный пароль

        // Неверный пароль
    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})
// Верный пароль

router.post("/", async (req, res) => {
    try {
        // Заменяем пароль на хешированный
        req.body.password = bcrypt.hashSync(req.body.password, salt)

        Users.create(req.body, async (error, data) => {
            if (error) {
                console.log(error);

                res.json({
                    ok: false,
                    message: "Error inside callback function WTF!",
                    error
                })
            } else {
                // Надо удалить пароль из объекта
                let newUser = JSON.parse(JSON.stringify(data))

                delete newUser.password

                res.json({
                    ok: true,
                    message: "Element patch!",
                    element: newUser
                })
            }
        })
    } catch (error) {
        console.log(error);

        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.delete("/:id", async (req, res) => {
    Users.findByIdAndDelete(req.params.id, async (error, data) => {
        if (error) {
            res.json({
                ok: false,
                messege: "Deleted shit!",
                el: data,
                error
            })
        } else {
            res.json({
                ok: true,
                message: 'Deleted',
                element: data,
            })
        }
    })
})
module.exports = router